#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
PS1='[\u@\h \W]\$ '
alias ll='ls -l'

if [ -d "$HOME/.local/bin" ]; then
    PATH="$HOME/.local/bin:$PATH"
fi



if [ -f ~/.local/lib/python3.4/site-packages/powerline/bindings/bash/powerline.sh ]; then
    source ~/.local/lib/python3.4/site-packages/powerline/bindings/bash/powerline.sh
fi
