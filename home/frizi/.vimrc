set nocompatible
filetype off
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

Plugin 'gmarik/Vundle.vim'
Plugin 'altercation/vim-colors-solarized'
Plugin 'Valloric/YouCompleteMe'
Plugin 'kien/ctrlp.vim'
Plugin 'scrooloose/syntastic'


call vundle#end()

filetype plugin indent on
syntax on
set background=dark
colorscheme solarized
"transparent solarized applied to terminal, do not use custom bg
hi Normal ctermbg=NONE

" powerline
set rtp+=$HOME/.local/lib/python3.4/site-packages/powerline/bindings/vim/
set laststatus=2
set t_Co=256
set timeoutlen=1000 ttimeoutlen=0

" Tab specific option
set tabstop=8                   "A tab is 8 spaces
set expandtab                   "Always uses spaces instead of tabs
set softtabstop=4               "Insert 4 spaces when tab is pressed
set shiftwidth=4                "An indent is 4 spaces
set shiftround                  "Round indent to nearest shiftwidth multiple

vmap <S-Tab> <gv
vmap <Tab> >gv

nmap <S-Tab> <<
imap <S-Tab> <Esc><<i

set ignorecase
set smartcase

function! s:swap_lines(n1, n2)
    let line1 = getline(a:n1)
    let line2 = getline(a:n2)
    call setline(a:n1, line2)
    call setline(a:n2, line1)
endfunction

function! s:swap_up()
    let n = line('.')
    if n == 1
        return
    endif

    call s:swap_lines(n, n - 1)
    exec n - 1
endfunction

function! s:swap_down()
    let n = line('.')
    if n == line('$')
        return
    endif

    call s:swap_lines(n, n + 1)
    exec n + 1
endfunction

nnoremap <silent> <A-S-Up> :call <SID>swap_up()<CR>
nnoremap <silent> <A-S-Down> :call <SID>swap_down()<CR>


:nmap <C-N><C-N> :set invnumber<CR>

let g:ycm_server_keep_logfiles = 1
let g:ycm_server_log_level = 'debug'


" types
au BufReadPost *.zsh-theme set syntax=zsh

