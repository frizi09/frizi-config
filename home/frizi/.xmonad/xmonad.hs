import XMonad
import XMonad.Layout
import XMonad.Util.Run
import XMonad.ManageHook
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.SetWMName
import XMonad.Layout.NoBorders
import XMonad.Hooks.UrgencyHook
import XMonad.Layout.Spacing
import XMonad.Hooks.Place
import XMonad.Actions.WindowBringer
import XMonad.Layout.IndependentScreens
import XMonad.Hooks.ManageHelpers
import XMonad.Layout.Renamed

import Data.Map    (fromList)
import Data.Monoid (mappend)

import Data.Maybe
import Data.Bits

import qualified XMonad.StackSet as W
import qualified Data.Map as M
import System.Exit

main = do
    -- spawn our left and right bars. in my case, I use two monitors,
    -- I want one bar on each, and my version of dzen supports the -xs
    -- argument for specifying on which screen to appear. if your
    -- situation is different in some way, use -w and -x to give your
    -- bars appriate width and x offsets for your needs.
    -- spawn $ "~/.scripts/initui"

    -- d <- spawnPipe "dzen2 -p -xs 1 -ta l -e 'onstart=lower' -fn xft:DejaVu:pixelsize=12 -h 16"
    d <- spawnPipe "~/.scripts/initui"
    -- spawn $ "conky -c ~/.xmonad/data/conky/dzen | " ++
    --         "dzen2 -p -xs 2 ta -r -e 'onstart=lower'"
    xmonad $ withUrgencyHook NoUrgencyHook $ defaultConfig
        { 
	  modMask = mod4Mask
	, terminal = "urxvt"
	, keys = myKeys

        , workspaces = withScreens 3 $ map (\c -> [c]) "αβγδεηλπω"

        , borderWidth = 2
        , normalBorderColor = "#000000"
        , focusedBorderColor = "#529100"

        , startupHook = setWMName "LG3D"
        , manageHook = myManageHook
        , layoutHook = myLayoutHook
	, logHook = myLogHook d
        }


dmenuOptions = "-fn DejaVu:pixelsize=11 -nb \"#000000\" -h 18 -i"
dmenuRun = "dmenu_run -hist ~/.dmenuhist -p \"$\"  " ++ dmenuOptions

volumeUnmuteAllCmd = "for i in Master Headphone Speaker ; do amixer -d pulse set -q $i unmute; done"
volumeToggleCmd = "amixer -D pulse set -q Master toggle"
volumeUpCmd = "amixer set -q Master 3- & " ++  volumeUnmuteAllCmd
volumeDownCmd = "amixer set -q Master 3+ & " ++  volumeUnmuteAllCmd

myKeys :: XConfig Layout -> M.Map (KeyMask, KeySym) (X ())
myKeys conf@(XConfig {XMonad.modMask = modMask}) = M.fromList $
    -- launching and killing programs
    [ ((modMask .|. shiftMask, xK_Return), spawn $ XMonad.terminal conf) -- %! Launch terminal
    , ((modMask,               xK_p     ), spawn dmenuRun) -- %! Launch dmenu
    , ((modMask .|. shiftMask, xK_c     ), kill) -- %! Close the focused window

    , ((modMask,               xK_space ), sendMessage NextLayout) -- %! Rotate through the available layout algorithms
    , ((modMask .|. shiftMask, xK_space ), setLayout $ XMonad.layoutHook conf) -- %!  Reset the layouts on the current workspace to default

    , ((modMask,               xK_n     ), refresh) -- %! Resize viewed windows to the correct size

    , ((modMask .|. shiftMask, xK_l     ), spawn "slimlock") -- locking screen

    , ((controlMask,           xK_Print ), spawn "sleep 0.2; scrot -s") -- capture screenshot of focused window       
    , ((0,                     xK_Print ), spawn "scrot") -- capture screenshot of full desktop  

    , ((modMask,               xK_d     ), spawn "thunar") -- open file manager

    -- sound management
    , ((0                    , 0x1008ff11), spawn volumeUpCmd)
    , ((0                    , 0x1008ff13), spawn volumeDownCmd)
    , ((0                    , 0x1008ff12), spawn volumeToggleCmd) -- XF86AudioMute

    -- move focus up or down the window stack
    , ((modMask,               xK_Tab   ), windows W.focusDown) -- %! Move focus to the next window
    , ((modMask .|. shiftMask, xK_Tab   ), windows W.focusUp  ) -- %! Move focus to the previous window
    , ((modMask,               xK_j     ), windows W.focusDown) -- %! Move focus to the next window
    , ((modMask,               xK_k     ), windows W.focusUp  ) -- %! Move focus to the previous window
    , ((modMask,               xK_m     ), windows W.focusMaster  ) -- %! Move focus to the master window

    -- modifying the window order
    , ((modMask,               xK_Return), windows W.swapMaster) -- %! Swap the focused window and the master window
    , ((modMask .|. shiftMask, xK_j     ), windows W.swapDown  ) -- %! Swap the focused window with the next window
    , ((modMask .|. shiftMask, xK_k     ), windows W.swapUp    ) -- %! Swap the focused window with the previous window

    -- windowbringer
    , ((modMask .|. shiftMask, xK_g     ), gotoMenu)
    , ((modMask .|. shiftMask, xK_b     ), bringMenu)
 
    -- resizing the master/slave ratio
    , ((modMask,               xK_h     ), sendMessage Shrink) -- %! Shrink the master area
    , ((modMask,               xK_l     ), sendMessage Expand) -- %! Expand the master area

    -- floating layer support
    , ((modMask,               xK_t     ), withFocused $ windows . W.sink) -- %! Push window back into tiling

    -- increase or decrease number of windows in the master area
    , ((modMask              , xK_comma ), sendMessage (IncMasterN 1)) -- %! Increment the number of windows in the master area
    , ((modMask              , xK_period), sendMessage (IncMasterN (-1))) -- %! Deincrement the number of windows in the master area

    -- quit, or restart
    , ((modMask .|. shiftMask, xK_q     ), io (exitWith ExitSuccess)) -- %! Quit xmonad
    , ((modMask              , xK_q     ), spawn "if type xmonad; then xmonad --recompile && xmonad --restart; else xmessage xmonad not in \\$PATH: \"$PATH\"; fi") -- %! Restart xmonad
    ]
    ++
    -- mod-[1..9] %! Switch to workspace N
    -- mod-shift-[1..9] %! Move client to workspace N
    [((m .|. modMask, k), windows $ onCurrentScreen f i)
        | (i, k) <- zip (workspaces' conf) [xK_1 .. xK_9]
        , (f, m) <- [(W.greedyView, 0), (W.shift, shiftMask)]]
    ++
    -- mod-{w,e,r} %! Switch to physical/Xinerama screens 1, 2, or 3
    -- mod-shift-{w,e,r} %! Move client to screen 1, 2, or 3
    [((m .|. modMask, key), screenWorkspace sc >>= flip whenJust (windows . f))
        | (key, sc) <- zip [xK_w, xK_e, xK_r] [0..]
        , (f, m) <- [(W.view, 0), (W.shift, shiftMask)]]
-- 
-- Loghook
-- 
-- note: some of these colors may differ from what's in the
-- screenshot, it changes daily
-- 
myLogHook h = dynamicLogWithPP . marshallPP 0 . myPP $ h 

myPP h = defaultPP
    -- display current workspace as darkgrey on light grey (opposite of 
    -- default colors)
    { ppCurrent         = dzenColor "#303030" "#909090" . pad 

    -- display other workspaces which contain windows as a brighter grey
    , ppHidden          = dzenColor "#909090" "" . pad 

    -- display other workspaces with no windows as a normal grey
    , ppHiddenNoWindows = dzenColor "#606060" "" . pad 

    -- display the current layout as a brighter grey
    , ppLayout          = dzenColor "#909090" "" . pad 

    -- if a window on a hidden workspace needs my attention, color it so
    , ppUrgent          = dzenColor "#ff0000" "" . pad . dzenStrip

    -- shorten if it goes over 100 characters
    , ppTitle           = shorten 100  

    -- no separator between workspaces
    , ppWsSep           = ""

    -- put a few spaces between each object
    , ppSep             = "  "

    -- output to the handle we were given as an argument
    , ppOutput          = hPutStrLn h
    }

-- add avoidStruts to your layoutHook like so

-- add manageDocks to your managehook like so 

mouseButtonDown bMask = ask >>= \w -> liftX $ (withDisplay $ \d -> io $ queryPointer d w)
                            >>= \(_,_,_,_,_,_,_,m) -> return $  m .&. bMask > 0

mouseButton1Down = mouseButtonDown button1Mask


myManageHook = (<+>) manageDocks $ composeAll . concat $ 
  [
    [className =? c --> doFloat | c <- myFloats ]
  , [title =? c --> doCenterFloat | c <- myTitleFloats ]
  , [className =? c --> doCenterFloat | c <- myCenterFloats ]
  , [className =? c --> doIgnore | c <- myIgnores ]
  , [className =? "Google-chrome-stable" <&&> mouseButton1Down --> doFloat]
  ]
  where
    myFloats = ["MPlayer"]
    myTitleFloats = ["Postęp działań na plikach"]
    myCenterFloats = ["Xfce4-power-manager-settings", "Wicd-client.py", "Clementine"]
    myIgnores = ["cairo-dock"]


myLayoutHook = smartBorders $ (avoidStruts $
        (named "tiled" tiled) |||
        (named "mirror" $ Mirror tiled) |||
        (named "full" Full)) ||| (named "fullscreen" Full)
    where
        named n = renamed [Replace n]
        tiled = smartSpacing 5 $ Tall nmaster delta ratio

        -- The default number of windows in the master pane
        nmaster = 1

        -- Default proportion of screen occupied by master pane
        ratio = 1/2

        -- Percent of screen to increment by when resizing panes
        delta = 3/100


